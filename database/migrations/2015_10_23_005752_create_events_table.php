<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    { // ['name', 'description', 'start', 'end', 'location', 'type']
      Schema::create('event_types', function(Blueprint $table) {
         $table->integer('id');
         $table->string('type');
         $table->string('color')->default("#86C441");
      });
      Schema::create('events', function (Blueprint $table) {
         $table->increments('id');
         $table->string('title');
         $table->string('description');
         $table->string('location');
         $table->boolean('allDay')->default(false);
         $table->dateTime('start');
         $table->dateTime('end');
         $table->string('url');
         $table->integer('event_type_id')->default(0);
         $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('event_types');
      Schema::dropIfExists('events');
    }
}
