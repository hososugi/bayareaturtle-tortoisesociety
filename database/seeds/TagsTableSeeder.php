<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Tag;

class TagsTableSeeder extends Seeder
{
    public function run()
    {
      DB::table('tags')->delete();

      Tag::create(array('tag' => 'club'));
      Tag::create(array('tag' => 'event'));
      Tag::create(array('tag' => 'party'));
      Tag::create(array('tag' => 'turtle'));
      Tag::create(array('tag' => 'tortoise'));
      Tag::create(array('tag' => 'habitat'));
    }
}
