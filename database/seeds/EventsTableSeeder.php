<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Event;
use Carbon\Carbon;

class EventsTableSeeder extends Seeder
{
    public function run()
    {
      DB::table('events')->delete();

      Event::create(array('title'=>'Meet & Greet',
                           'description'=>'Come and meet our officers and all the other members.',
                           'location'=>'San Jose Masonic Center 2500 Masonic Dr, San Jose, CA 95125',
                           'allDay'=>false,
                           'start'=>(Carbon::create(2016,1,15,19)),
                           'end'=>(Carbon::create(2016,1,15,21)),
                           'event_type_id'=>0));

      Event::create(array('title'=>'Speaker: Eliza Trickett',
                           'description'=>'Eliza Trickett from Mazuri Foods',
                           'location'=>'San Jose Masonic Center 2500 Masonic Dr, San Jose, CA 95125',
                           'allDay'=>false,
                           'start'=>(Carbon::create(2016,2,19,19)),
                           'end'=>(Carbon::create(2016,2,19,21)),
                           'event_type_id'=>0));

      Event::create(array('title'=>'Presentation by Steven Sifuentes',
                           'description'=>'',
                           'location'=>'San Jose Masonic Center 2500 Masonic Dr, San Jose, CA 95125',
                           'allDay'=>false,
                           'start'=>(Carbon::create(2016,3,18,19)),
                           'end'=>(Carbon::create(2016,3,18,21)),
                           'event_type_id'=>0));

      Event::create(array('title'=>'Workshop: Building a tortoise house',
                           'description'=>'',
                           'location'=>'San Jose Masonic Center 2500 Masonic Dr, San Jose, CA 95125',
                           'allDay'=>false,
                           'start'=>(Carbon::create(2016,4,15,19)),
                           'end'=>(Carbon::create(2016,4,15,21)),
                           'event_type_id'=>0));

      Event::create(array('title'=>'Presentation: Chris Leone',
                           'description'=>'Chris Leone\'s presentation, as seen at the Turtle Survival Alliance 2015 Symposium on the Conservation and Biology of Tortoises and Freshwater Turtles in Tucson, AZ, August 6-9. This talk was part of the Captive Husbandry: Collaboration and Conservation session held on Saturday, August 8th, 2015',
                           'location'=>'San Jose Masonic Center 2500 Masonic Dr, San Jose, CA 95125',
                           'allDay'=>false,
                           'start'=>(Carbon::create(2016,5,20,19)),
                           'end'=>(Carbon::create(2016,5,20,21)),
                           'event_type_id'=>0));

      Event::create(array('title'=>'Pot Luck',
                           'description'=>'A social night where everyone can bring their animals to show.',
                           'location'=>'San Jose Masonic Center 2500 Masonic Dr, San Jose, CA 95125',
                           'allDay'=>false,
                           'start'=>(Carbon::create(2016,6,17,19)),
                           'end'=>(Carbon::create(2016,6,17,21)),
                           'event_type_id'=>0));

      Event::create(array('title'=>'Presentation by Terry & Nadia',
                           'description'=>'It\'s Gus\'s birthday on August 6th. Come celebrate.',
                           'location'=>'San Jose Masonic Center 2500 Masonic Dr, San Jose, CA 95125',
                           'allDay'=>false,
                           'start'=>(Carbon::create(2016,7,15,19)),
                           'end'=>(Carbon::create(2016,7,15,21)),
                           'event_type_id'=>0));

      Event::create(array('title'=>'Presentation by Johnny Rodriguez',
                           'description'=>'Burrow construction with concrete blocks.',
                           'location'=>'San Jose Masonic Center 2500 Masonic Dr, San Jose, CA 95125',
                           'allDay'=>false,
                           'start'=>(Carbon::create(2016,8,19,19)),
                           'end'=>(Carbon::create(2016,8,19,21)),
                           'event_type_id'=>0));

      Event::create(array('title'=>'Presentation: Preparing for hibernation',
                           'description'=>'',
                           'location'=>'San Jose Masonic Center 2500 Masonic Dr, San Jose, CA 95125',
                           'allDay'=>false,
                           'start'=>(Carbon::create(2016,9,16,19)),
                           'end'=>(Carbon::create(2016,9,16,21)),
                           'event_type_id'=>0));

      Event::create(array('title'=>'Club meeting',
                           'description'=>'',
                           'location'=>'San Jose Masonic Center 2500 Masonic Dr, San Jose, CA 95125',
                           'allDay'=>false,
                           'start'=>(Carbon::create(2016,10,21,19)),
                           'end'=>(Carbon::create(2016,10,21,21)),
                           'event_type_id'=>0));

      Event::create(array('title'=>'Club meeting',
                           'description'=>'',
                           'location'=>'San Jose Masonic Center 2500 Masonic Dr, San Jose, CA 95125',
                           'allDay'=>false,
                           'start'=>(Carbon::create(2016,11,18,19)),
                           'end'=>(Carbon::create(2016,11,18,21)),
                           'event_type_id'=>0));

      Event::create(array('title'=>'Christmas party',
                           'description'=>'',
                           'location'=>'San Jose Masonic Center 2500 Masonic Dr, San Jose, CA 95125',
                           'allDay'=>false,
                           'start'=>(Carbon::create(2016,12,16,19)),
                           'end'=>(Carbon::create(2016,12,16,21)),
                           'event_type_id'=>0));

    }
}
