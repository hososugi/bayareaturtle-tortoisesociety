<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Resource;
use Carbon\Carbon;

class ResourcesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('resources')->delete();

      Resource::create(array('title'=>'Care of adult tortoises',
                           'description'=>'Care of adult tortoises',
                           'url'=>'./docs/Care of adult tortoises.pdf',
                           'tags'=>'care|tortoises|adult'));

      Resource::create(array('title'=>'Diet and healthy growth',
                           'description'=>'Diet and healthy growth',
                           'url'=>'./docs/Diet and healthy growth.pdf',
                           'tags'=>'care|diet'));

      Resource::create(array('title'=>'Edible plants',
                           'description'=>'Edible plants',
                           'url'=>'./docs/edible plants.pdf',
                           'tags'=>'care|diet|plants'));

      Resource::create(array('title'=>'Husbandry through the year',
                           'description'=>'Husbandry through the year',
                           'url'=>'./docs/Husbandry through the year.pdf',
                           'tags'=>''));

      Resource::create(array('title'=>'Natural Rearing of Tortoises, Accommodation',
                           'description'=>'Natural Rearing of Tortoises, Accommodation',
                           'url'=>'./docs/Natural Rearing of Tortoises, Accommodation.pdf',
                           'tags'=>'tortoises'));

      Resource::create(array('title'=>'Plant Photos',
                           'description'=>'Plant Photos',
                           'url'=>'./docs/Plant Photos.pdf',
                           'tags'=>'care|diet'));

      Resource::create(array('title'=>'Seed Planting for Sulcatas',
                           'description'=>'Seed Planting for Sulcatas',
                           'url'=>'./docs/Seed Planting for Sulcatas.pdf',
                           'tags'=>'care|diet|plants|sulcatas'));

      Resource::create(array('title'=>'Sulcata Food The perfect seed mixes and plants for your tortoises Serving pet owners, breeders, and zoos since 1998',
                           'description'=>'Sulcata Food The perfect seed mixes and plants for your tortoises Serving pet owners, breeders, and zoos since 1998',
                           'url'=>'./docs/Sulcata Food The perfect seed mixes and plants for your tortoises Serving pet owners, breeders, and zoos since 1998.pdf',
                           'tags'=>'care|diet|plants|sulcatas'));

      Resource::create(array('title'=>'The Guide To Rearing Mediterranean Tortoises ',
                           'description'=>'The Guide To Rearing Mediterranean Tortoises ',
                           'url'=>'./docs/The Guide To Rearing Mediterranean Tortoises .pdf',
                           'tags'=>'tortoises'));

      Resource::create(array('title'=>'Tortoises in the Wild',
                           'description'=>'Tortoises in the Wild',
                           'url'=>'./docs/Tortoises in the Wild.pdf',
                           'tags'=>'tortoises'));
    }
}
