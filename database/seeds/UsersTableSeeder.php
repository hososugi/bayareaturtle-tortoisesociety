<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\User;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
      DB::table('users')->delete();

      User::create(array(
         'firstname' => 'Johnny',
         'lastname' => 'Rodriguez',
         'email' => 'BayAreaTTS@gmail.com',
         'password' => Hash::make('Sulton1985'),
         'active' => 1,
      ));
    }
}
