<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Image;

class ImagesTableSeeder extends Seeder
{
    public function run()
    {
      //DB::table('images')->delete();

      Image::create(array('url' => './images/gallery/what+do+eastern+box+turtleseat.jpg',
                          'description' => 'Lorem ipsum dolor sit amet, ac ullamcorper et aliquam, faucibus elit cras massa volutpat, etiam ultricies ut aenean aliquam aptent cursus, eros pharetra tempus aliquam quis sollicitudin.',
                          'tags' => 'turtle'));

      Image::create(array('url' => './images/gallery/EasternBoxTurtleMale.jpg',
                          'description' => 'Lorem ipsum dolor sit amet, ac ullamcorper et aliquam, faucibus elit cras massa volutpat, etiam ultricies ut aenean aliquam aptent cursus, eros pharetra tempus aliquam quis sollicitudin.',
                          'tags' => 'turtle'));

      Image::create(array('url' => './images/gallery/Terrapene_ornata_male.jpg',
                          'description' => 'Lorem ipsum dolor sit amet, ac ullamcorper et aliquam, faucibus elit cras massa volutpat, etiam ultricies ut aenean aliquam aptent cursus, eros pharetra tempus aliquam quis sollicitudin.',
                          'tags' => 'turtle'));

      Image::create(array('url' => './images/gallery/14526715971_0b853310d6_z.jpg',
                          'description' => 'Lorem ipsum dolor sit amet, ac ullamcorper et aliquam, faucibus elit cras massa volutpat, etiam ultricies ut aenean aliquam aptent cursus, eros pharetra tempus aliquam quis sollicitudin.',
                          'tags' => 'turtle'));

      Image::create(array('url' => './images/gallery/_64140145_01295586.jpg',
                          'description' => 'Lorem ipsum dolor sit amet, ac ullamcorper et aliquam, faucibus elit cras massa volutpat, etiam ultricies ut aenean aliquam aptent cursus, eros pharetra tempus aliquam quis sollicitudin.',
                          'tags' => 'turtle'));

      Image::create(array('url' => './images/gallery/Aldabra_Giant_Tortoise_Geochelone_gigantea_edit1.jpg',
                          'description' => 'Lorem ipsum dolor sit amet, ac ullamcorper et aliquam, faucibus elit cras massa volutpat, etiam ultricies ut aenean aliquam aptent cursus, eros pharetra tempus aliquam quis sollicitudin.',
                          'tags' => 'turtle'));

      Image::create(array('url' => './images/gallery/0308-Tortoises-W(1).jpg',
                          'description' => 'Lorem ipsum dolor sit amet, ac ullamcorper et aliquam, faucibus elit cras massa volutpat, etiam ultricies ut aenean aliquam aptent cursus, eros pharetra tempus aliquam quis sollicitudin.',
                          'tags' => 'tortoise|sulcata'));

      Image::create(array('url' => './images/gallery/african-spurred-tortoise1.jpg',
                          'description' => 'Lorem ipsum dolor sit amet, ac ullamcorper et aliquam, faucibus elit cras massa volutpat, etiam ultricies ut aenean aliquam aptent cursus, eros pharetra tempus aliquam quis sollicitudin.',
                          'tags' => 'tortoise|sulcata'));

      Image::create(array('url' => './images/gallery/African-Sulcata-Tortoise.jpg',
                          'description' => 'Lorem ipsum dolor sit amet, ac ullamcorper et aliquam, faucibus elit cras massa volutpat, etiam ultricies ut aenean aliquam aptent cursus, eros pharetra tempus aliquam quis sollicitudin.',
                          'tags' => 'tortoise|sulcata'));

      Image::create(array('url' => './images/gallery/ceec48a5355c682c324450b66b033515.jpg',
                          'description' => 'Lorem ipsum dolor sit amet, ac ullamcorper et aliquam, faucibus elit cras massa volutpat, etiam ultricies ut aenean aliquam aptent cursus, eros pharetra tempus aliquam quis sollicitudin.',
                          'tags' => 'tortoise|sulcata'));

      Image::create(array('url' => './images/gallery/DSCN2975.jpg',
                          'description' => 'Lorem ipsum dolor sit amet, ac ullamcorper et aliquam, faucibus elit cras massa volutpat, etiam ultricies ut aenean aliquam aptent cursus, eros pharetra tempus aliquam quis sollicitudin.',
                          'tags' => 'tortoise|sulcata'));

      Image::create(array('url' => './images/gallery/Geochelone_sulcata_-Oakland_Zoo_-feeding-8a.jpg',
                          'description' => 'Lorem ipsum dolor sit amet, ac ullamcorper et aliquam, faucibus elit cras massa volutpat, etiam ultricies ut aenean aliquam aptent cursus, eros pharetra tempus aliquam quis sollicitudin.',
                          'tags' => 'tortoise|sulcata'));

      Image::create(array('url' => './images/gallery/p00yfk3x.jpg',
                          'description' => 'Lorem ipsum dolor sit amet, ac ullamcorper et aliquam, faucibus elit cras massa volutpat, etiam ultricies ut aenean aliquam aptent cursus, eros pharetra tempus aliquam quis sollicitudin.',
                          'tags' => 'tortoise|sulcata'));

      Image::create(array('url' => './images/gallery/sulcata-tortoise-tank.jpg',
                          'description' => 'Lorem ipsum dolor sit amet, ac ullamcorper et aliquam, faucibus elit cras massa volutpat, etiam ultricies ut aenean aliquam aptent cursus, eros pharetra tempus aliquam quis sollicitudin.',
                          'tags' => 'tortoise|sulcata'));

      Image::create(array('url' => './images/gallery/sulcata-tortoise.jpg',
                          'description' => 'Lorem ipsum dolor sit amet, ac ullamcorper et aliquam, faucibus elit cras massa volutpat, etiam ultricies ut aenean aliquam aptent cursus, eros pharetra tempus aliquam quis sollicitudin.',
                          'tags' => 'tortoise|sulcata'));
    }
}
