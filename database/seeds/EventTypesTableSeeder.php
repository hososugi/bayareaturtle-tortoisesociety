<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\EventType;

class EventTypesTableSeeder extends Seeder
{
    public function run()
    {
      DB::table('event_types')->delete();

      EventType::create(array('id' => 0,
                          'type' => 'club',
                          'color' => '#87c440'));

      EventType::create(array('id' => 1,
                           'type' => 'event',
                           'color' => '#E24067'));

      EventType::create(array('id' => 2,
                           'type' => 'party',
                           'color' => '#2da4c2'));
    }
}
