<?php

try{
   require_once dirname(__FILE__) . '\..\vendor\oyejorge\less.php\lib\Less\Autoloader.php';
   Less_Autoloader::register();

   $options = array( 'compress'=>false );
   $parser = new Less_Parser( $options );
   $parser->parseFile( '../resources/assets/less/app.less', '/resources/assets/css' );
   $css = $parser->getCss();

   $to_cache = array( '../resources/assets/less/app.less' => '/resources/assets/css' );
   Less_Cache::$cache_dir = '/resources/assets/css';
   $css_file_name = Less_Cache::Get( $to_cache );
   $compiled = file_get_contents( '/var/www/writable_folder/'.$css_file_name );
}catch(Exception $e){
   $error_message = $e->getMessage();
}
