
$(document).ready(function(){
   setupImageUpload();
   setupImagesTable();
   setupImageEditing();
   setupImageDeletion();
});

function setupImageUpload(){
   $('#image-upload-form').on('submit',(function(e) {
      e.preventDefault();

      var size = $("#image-upload-input")[0].files[0].size;
      var maxSize = parseInt($("[name='MAX_FILE_SIZE']").val(), 10);

      if(size < maxSize){
        var formData = new FormData(this);

        $.ajax({
            type:'POST',
            url: $(this).attr('action'),
            data:formData,
            cache:false,
            contentType: false,
            processData: false,
            success:function(data){
               if(data.hasOwnProperty('success')){
                  var firstRow = $("#admin-images-table").find('tbody > tr').first();
                  var lastRow = $("#admin-images-table").find('tbody > tr').last();

                  // Set up the new row.
                  var newRow = firstRow.clone();
                  newRow.find('td').not(':first-child').not(':last-child').empty();
                  newRow.find('td:first-child').find('img').attr('src', data.data.url);
                  newRow.find('.glyphicon-pencil').data({
                     id: data.data.id,
                     tags: "",
                     description: "",
                     url: data.data.url
                  });
                  newRow.find('.glyphicon-trash').data('id', data.data.id);

                  newRow.insertBefore(firstRow);

                  // Remove the last row to keep the correct amount of images.
                  lastRow.remove();
               }
            },
            error: function(data){
                console.log("error");
                console.log(data);
            }
        });
     }
     else{
        console.error('file too large.');
     }

    }));

    $("#image-upload-input").on("change", function() {
        $("#image-upload-form").submit();
    });
}

function setupImagesTable(){

}

function setupImageEditing(){
   $("#admin-images-table").on('click', '.glyphicon-pencil', function(){
      $('.active-edit').removeClass('active-edit');
      $(this).addClass('active-edit');
      var data = $(this).data();

      // Set up the new image info.
      $("#modal-edit-image-img").attr('src', data.url);
      $("#model-edit-image-tags").val(data.tags.split('|').join(', '));
      $("#model-edit-image-description").val(data.description);

      $("#modal-update-image-button").attr('disabled', true);

      // Show the dialog.
      $("#modal-edit-image").modal('show');
   });

   $("#model-edit-image-tags,#model-edit-image-description").on('change keypress',function(){
      $("#modal-update-image-button").attr('disabled', false);
   });

   $("#modal-update-image-button").click(function(){
      var tags = $("#model-edit-image-tags").val();
      var tagsArray = tags.split(',');

      for(var i = 0; i < tagsArray.length; i++)
         tagsArray[i] = tagsArray[i].trim();

      var newTags = tagsArray.join('|');

      var data = {
         id: $(".active-edit").data('id'),
         tags: newTags,
         description: $("#model-edit-image-description").val()
      };

      $.ajax({
         headers: {
            'X-XSRF-TOKEN' : csrf
         },
         type:'PATCH',
         url: '../image',
         cache:false,
         data: data,
         patchData: data,
         success:function(data){
            if(data.hasOwnProperty('success')){
               $("#modal-edit-image").modal('hide');

               var row = $('.active-edit').closest('tr');
               row.children('td:eq(1)').html(this.patchData.description);
               row.children('td:eq(2)').html(this.patchData.tags);
            }
         },
         error: function(data){
            console.log("--- Error: "+JSON.stringify(data));
         }
      });
   });
}

function setupImageDeletion(){
   $("#admin-images-table").on('click', '.glyphicon-trash', function(){
      $('.active-removal').removeClass('active-removal');
      $(this).addClass('active-removal');

      $("#modal-remove-image").modal('show');
   });

   $('#modal-remove-image-button').click(function(){
      var imageID = $(".active-removal").data('id');

      $.ajax({
         headers: {
            'X-XSRF-TOKEN' : csrf
         },
         type:'DELETE',
         url: '../image',
         cache:false,
         data: {
            id: imageID
         },
         success:function(data){
            if(data.hasOwnProperty('success')){
               $("#modal-remove-image").modal('hide');
               $('.active-removal').closest('tr').remove();
            }
         },
         error: function(data){
            console.log("--- Error: "+JSON.stringify(data));
         }
      });
   });
}
