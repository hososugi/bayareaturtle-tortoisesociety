var limit = 10;


$(document).ready(function(){
   setupSearch();
   setupLoadMore();
});

function setupSearch(){
   $("#gallery-search-input").autocomplete({
      source: window.tags,
      select: function(event, ui) {
         $("#gallery-search-icon").click();
      }
   });

   $("#gallery-search-icon").click(function(){
      var terms = $("#gallery-search-input").val();
      var data = {
         terms: terms,
         limit: limit,
         offset: 0
      };

      $("#gallery-search-input").autocomplete("close");

      $.ajax({
         url: './allImages',
         cache: false,
         method: 'GET',
         data: data,
         passedData: data,
         success: function(data){
            data.passedData = this.passedData;

            $("#gallery-images-container").empty();
            populateGallery(data);
         },
         error: function(){
            console.error('/allImages ajax error.');
         }
      });
   });

   $("#gallery-search-input").keypress(function(event){
      if(event.which == 13){
         $("#gallery-search-icon").click();
      }
   });
}

function setupLoadMore(){
   $("#gallery-load-more").click(function(){
      var data = {
         terms: window.terms,
         limit: window.limit,
         offset: (window.loadedCount)
      };

      $.ajax({
         url: './allImages',
         cache: false,
         method: 'GET',
         data: data,
         passedData: data,
         success: function(data){
            data.passedData = this.passedData;

            populateGallery(data);
         },
         error: function(){
            console.error('/allImages ajax error.');
         }
      });
   });
}

function populateGallery(data){
   window.totalCount = data.total_count;
   window.loadedCount = (data.passedData.offset) + data.images.length;
   window.terms = data.passedData.terms;

   $("#gallery-search-count").html(loadedCount+" of "+data.total_count+" results");

   for(image in data.images){
      var wrapper = $("<div/>", {
         class: 'moment-wrapper'
      }).appendTo('#gallery-images-container');

      $("<img/>",{
         class: 'moment-image',
         src: data.images[image].url
      }).appendTo(wrapper);
   }

   // Check the Load More button.
   if(loadedCount >= totalCount)
      $("#gallery-load-more").addClass('disabled');
   else
      $("#gallery-load-more").removeClass('disabled');
}
