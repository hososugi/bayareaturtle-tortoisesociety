/*
   Detects if a mobile device.
   Usage: isMobile.any();
*/
var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};

var Months = [
   "January",
   "February",
   "March",
   "April",
   "May",
   "June",
   "July",
   "August",
   "September",
   "October",
   "November",
   "December"
];
var GoogleGeocoder;
var lastAddress;
var lastAddressLatLon;
var map;

function initializeMap() {
   GoogleGeocoder = new google.maps.Geocoder();

   var mapCanvas = document.getElementById('event-viewer-map');
   var mapOptions = {
      center: lastAddressLatLon,
      zoom: 16,
      mapTypeId: google.maps.MapTypeId.ROADMAP
   }
   map = new google.maps.Map(mapCanvas, mapOptions);
}

function setMapCenter(newAddress) {
   if(newAddress != lastAddress) {
      if($('#event-viewer-map').html() == "")
         initializeMap();

      GoogleGeocoder.geocode( { 'address': newAddress}, function(results, status) {
         if(status == google.maps.GeocoderStatus.OK) {
            lastAddressLatLon = results[0].geometry.location;

            map.setCenter(results[0].geometry.location);

            var marker = new google.maps.Marker({
               map: map,
               position: results[0].geometry.location
            });
         }
         else {
            alert("There was an error while generating the map. Please email the admin at BayAreaTortugaSociety@gmail.com. Reason: " + status);
         }
      });

      lastAddress = newAddress;
   }
}


function showEventDetails(eventIndex) {
   var event = events[eventIndex] || null;
   if(event) {
      $("#event-viewer-title").html(event.title);
      var dateString;
      var startArray = event.start.split(" ");
      var startDate = new Date(startArray[0]+"T"+startArray[1]+"Z");
      var endArray = event.end.split(" ");
      var endDate   = new Date(endArray[0]+"T"+endArray[1]+"Z");

      var startHour = startDate.getHours();
      var startMinutes = startDate.getMinutes();
      dateString = Months[startDate.getMonth()].substr(0,3) + " " +
                  startDate.getDate() + " @ " +
                  ((startHour > 12)? startHour - 12 : startHour) + ":" +
                  ("0"+startMinutes).substr(startMinutes.length-2) +
                  ((startHour > 12)? "PM" : "AM");

      $("#event-viewer-date").html(dateString);
      $("#event-viewer-description").html(event.description);
      $("#event-viewer-location").attr("href", "https://www.google.com/maps/search/"+event.location);
      $("#event-viewer-location-text").html(event.location);

      $("#event-viewer").modal('show');

      $('#event-viewer').on('shown.bs.modal', function(e) {
         setMapCenter(event.location);
      }).modal("show");

   }
}

function showImageDetails(image) {
   // Get the data for the clicked image.
   var data = $(image).data();

   // Update which thumbnail is actively highlighted.
   $("#gallery-images-container").find('.moment-wrapper.active').removeClass('active');
   $(this).addClass('active');

   // Update the modal data.
   $("#image-viewer-img").attr('src', data.url);
   $("#image-viewer-tags").html(data.tags.split('|').join(', '));
   $("#image-viewer-description").html(data.description);

   // Show the modal.
   $("#image-viewer").modal("show");
}
