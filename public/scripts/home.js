
$(document).ready(function(){
   setupMomentBubbleButtons();
});


function setupMomentBubbleButtons(){
   $("#home-moment-bubble-buttons").on('click', '.moment-bubble-button', function(event){
      // Update the active button.
      $("#home-moment-bubble-buttons").find('.moment-bubble-button.active').removeClass('active');
      $(this).addClass('active');

      // Get the image's left.
      var buttonIndex = $(this).prevAll().length;
      var image = $("#home-moments").children('.moment-wrapper:eq('+buttonIndex+')');
      var newLeft = -image.position().left;

      // Move the images.
      $("#home-moments").animate({
         left: newLeft
      }, "fast");
   });
}
