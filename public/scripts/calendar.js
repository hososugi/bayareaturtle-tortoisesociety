
$(document).ready(function(){
   $('#calendar').fullCalendar({
      header: {
         left:   'prev,next today',
         center: 'title',
         right:  'eventButton clubButton'
      },
      customButtons: {
         clubButton: {
            text: 'Club',
            click: function() {
               $(this).toggleClass('events-hidden-button');
               $(".calendar-event-club").toggle("fast");
            }
         },
         eventButton: {
            text: 'Event',
            click: function() {
               $(this).toggleClass('events-hidden-button');
               $(".calendar-event-event").toggle("fast");
            }
         }
      },
      events: events,
      eventClick: function(calEvent, jsEvent, view) {
         console.debug("event index: "+calEvent.index);
         showEventDetails(calEvent.index);
      }
   });
});
