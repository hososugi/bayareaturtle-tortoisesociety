<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
   protected $table = 'events';
   protected $fillable = ['title', 'description', 'location', 'allDay', 'start', 'end', 'event_type_id'];
   protected $guarded = array('id');
}
