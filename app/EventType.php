<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventType extends Model
{
   protected $table = 'event_types';
   public $timestamps = false;
   protected $fillable = ['type', 'color'];
   protected $guarded = array('id');
}
