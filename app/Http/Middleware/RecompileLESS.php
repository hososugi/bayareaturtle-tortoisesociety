<?php

namespace App\Http\Middleware;

use Closure;
use Langemike\Laravel5Less\LessFacade as Less;

class RecompileLESS
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
         Less::compile('app');
         Less::compile('app', ['compress'=>true, 'public_path'=>public_path('min-css')]);

         return $next($request);
    }
}
