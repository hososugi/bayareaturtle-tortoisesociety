<?php
namespace App\Http\Middleware;

use Log;
use Closure;
use Illuminate\Support\Facades\Request;

class MaxFileSize
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      // Check if upload has exceeded max size limit
      if (!(Request::isMethod('POST') or Request::isMethod('PUT'))) {
         return $next($request);
      }

      // Get the max upload size (in Mb, so convert it to bytes)
      $maxUploadSize = 1024 * 1024 * ini_get('post_max_size');
      $contentSize = 0;

      if (isset($_SERVER['HTTP_CONTENT_LENGTH']))
      {
         $contentSize = $_SERVER['HTTP_CONTENT_LENGTH'];
      }
      elseif (isset($_SERVER['CONTENT_LENGTH']))
      {
         $contentSize = $_SERVER['CONTENT_LENGTH'];
      }

      // If content exceeds max size, throw an exception
      Log::info("contentSize/maxUploadSize: ".$contentSize."/".$maxUploadSize);
      if ($contentSize > $maxUploadSize)
      {
         abort(403, 'File size too large.');
         //throw new GSVnet\Core\Exceptions\MaxUploadSizeException;
      }

      return $next($request);
    }
}
