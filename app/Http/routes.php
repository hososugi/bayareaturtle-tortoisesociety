<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
use Illuminate\Http\Request;
use Carbon\Carbon;

//Route::get('/', 'MainController@showHome');
Route::get('/', function(){
   $images = App\Image::orderBy('created_at', 'desc')->take(3)->get();
   $events = App\Event::where('start', '>', Carbon::today())->orderBy('start')->take(3)->get();
   $resources = App\Resource::orderBy('click_count')->take(3)->get();

   return view('home', [
      'header_tab'=>'home',
      'images'=>$images,
      'events'=>$events,
      'resources'=>$resources
   ]);
});

Route::get('/gallery', function(){

   $toLoadCount = 20;
   if(Agent::isMobile())
      $toLoadCount = 10;

   $allImages = App\Image::orderBy('created_at', 'DESC')->get();
   $totalCount = $allImages->count();
   $loadedCount = ($totalCount < $toLoadCount)? $totalCount : $toLoadCount;
   $images = $allImages->take($toLoadCount);
   $tags = App\Tag::orderBy('tag')->get();
   $tagsArray = array();

   foreach($tags as $tag){
      array_push($tagsArray, $tag->tag);
   }

   JavaScript::put([
      'tags' => $tagsArray,
      'totalCount'=>$totalCount,
      'loadedCount'=>$loadedCount,
      'terms'=>''
    ]);

   return view('gallery', [
      'header_tab'=>'gallery',
      'images'=>$images,
      'totalCount'=>$totalCount,
      'loadedCount'=>$loadedCount,
      'tags'=>$tagsArray
   ]);
});

Route::get('/calendar', function(){
   $events = App\Event::where('start', '>=', Carbon::today())->orderBy('start')->get();
   $eventTypes = App\EventType::orderBy('id', 'ASC')->get();

   foreach($events as $key=>$event){
      $event->index = $key;
      $event->className = 'calendar-event-'.$eventTypes[$event->event_type_id]->type;
   }

   return view('calendar', [
      'header_tab'=>'calendar',
      'events'=>$events,
      'eventTypes'=>$eventTypes
   ]);
});

Route::get('/resources', function(){

   $toLoadCount = 20;
   if(Agent::isMobile())
      $toLoadCount = 10;

   $allResources = App\Resource::orderBy('title', 'ASC')->get();
   $totalCount = $allResources->count();
   $loadedCount = ($totalCount < $toLoadCount)? $totalCount : $toLoadCount;
   $resources = $allResources->take($toLoadCount);
   $tags = App\Tag::orderBy('tag')->get();
   $tagsArray = array();

   foreach($tags as $tag){
      array_push($tagsArray, $tag->tag);
   }

   JavaScript::put([
      'tags' => $tagsArray,
      'totalCount'=>$totalCount,
      'loadedCount'=>$loadedCount,
      'terms'=>''
    ]);

   return view('resources', [
      'header_tab'=>'resources',
      'resources'=>$resources,
      'totalCount'=>$totalCount,
      'loadedCount'=>$loadedCount,
      'tags'=>$tagsArray
   ]);
});

Route::get('/about', function(){

   return view('about', [
      'header_tab'=>'about'
   ]);
});

Route::get('/admin', ['middleware' => 'auth', function(){
   return view('admin.admin_dashboard', [
      'header_tab'=>'admin',
      'header_admin_tab'=>'dashboard',
      'images'=>[],
      'max_file_size'=>0,
      'totalCount'=>0,
      'loadedCount'=>0,
      'tags'=>[]
   ]);
}]);

Route::get('/admin/images', ['middleware' => 'auth', function(){

   $toLoadCount = 20;
   if(Agent::isMobile())
      $toLoadCount = 10;

   $allImages = App\Image::orderBy('created_at', 'DESC')->get();//all();
   $totalCount = $allImages->count();
   $loadedCount = ($totalCount < $toLoadCount)? $totalCount : $toLoadCount;
   $images = $allImages->take($toLoadCount);
   $tags = App\Tag::orderBy('tag')->get();
   $tagsArray = array();

   foreach($tags as $tag){
      array_push($tagsArray, $tag->tag);
   }

   JavaScript::put([
      'tags' => $tagsArray,
      'totalCount'=>$totalCount,
      'loadedCount'=>$loadedCount,
      'terms'=>''
    ]);

   return view('admin.admin_images', [
      'header_tab'=>'admin',
      'header_admin_tab'=>'images',
      'max_file_size'=>toByteSize(ini_get('post_max_size')),
      'images'=>$images,
      'totalCount'=>$totalCount,
      'loadedCount'=>$loadedCount,
      'tags'=>$tagsArray
   ]);
}]);

Route::get('/admin/events', ['middleware' => 'auth', function(){
   return view('admin.admin_events', [
      'header_tab'=>'admin',
      'header_admin_tab'=>'events'
   ]);
}]);

Route::get('/admin/resources', ['middleware' => 'auth', function(){
   return view('admin.admin_resources', [
      'header_tab'=>'admin',
      'header_admin_tab'=>'resources'
   ]);
}]);


Route::post('/admin/imageUpload', ['as'=> 'admin.imageUpload', 'middleware' => ['auth','MaxUploadFileSize'], function(Request $request) {

   Log::error("/admin/imageUpload check ");
   if (Auth::check()) {
     $file = array('image' => Input::file('image'));
     $rules = array('image' => 'required|image|mimes:jpeg,bmp,png|max:2000'); //mimes:jpeg,bmp,png and for max size max:10000
     $validator = Validator::make($file, $rules);

     if ($validator->fails()) {

       Log::info('--- Validation failed.');
       return $validator->errors()->all();
     }
     else {
       if (Input::file('image')->isValid()) {
         $destinationPath = 'images/gallery';
         $extension = Input::file('image')->getClientOriginalExtension();

         list($usec, $sec) = explode(" ", microtime());
         $time = ((float)$usec + (float)$sec);

         $fileName = $time.'.'.$extension;
         Input::file('image')->move($destinationPath, $fileName);

         // Create the new data in the database.
         $imageData = [
            'url'=>'./'.$destinationPath.'/'.$fileName
         ];

         $newImage = App\Image::create($imageData);
         $newImage->save();

         Session::flash('success', 'Upload successfully');
         return [
            'success' => "The image was uploaded successfully.",
            'data'=> [
               'id'=> $newImage->id,
               'url'=>url('/').'/'.$destinationPath.'/'.$fileName
            ]
         ];
       }
       else {
         Session::flash('error', 'uploaded file is not valid');
         return ['error' => 'uploaded file is not valid'];
       }
     }
  }
  else {
     return ["error" => "you must be logged in."];
 }
}]);

// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

Route::delete('image', function(){
   if(Input::has('id')){
      $img = App\Image::find(Input::get('id'));
      if(count($img) > 0){
         $filename = substr($img->url, 2);

         // Delete the file.
         unlink($filename);

         // Remove from the database.
         $img->delete();

         return ["success"=>"The image was removed."];
      }
      else{
         return ["fail"=>"There was no image with that ID."];
      }
   }
   else{
      return ["fail"=>"An image ID must be passed."];
   }

});

Route::patch('image', function(){
   if(Input::has('id')){
      $img = App\Image::find(Input::get('id'));

      if(Input::has('tags')){
         $img->tags = Input::get('tags');
      }
      if(Input::has('description')){
         $img->description = Input::get('description');
      }

      $img->save();

      return ['success'=>"The image was updated."];
   }
   else{
      return ["fail"=>"An image ID must be passed."];
   }

});

// --- Support routes.
Route::get('allImages', function(){
   $images = App\Image::all();
   $count = $images->count();


   if(Input::has('offset') and Input::has('limit')){
      $offset = (int) Input::get("offset");
      $limit = (int) Input::get("limit");

      if(Input::has('terms')){
         $terms = Input::get('terms');
         $images = App\Image::whereRaw('tags REGEXP ?', array($terms))->take($limit)->skip($offset)->get();
         $count = App\Image::whereRaw('tags REGEXP ?', array($terms))->count();
      }
      else {
         $images = App\Image::take($limit)->skip($offset)->get();
         $count = App\Image::all()->count();
      }
   }
   else if(Input::has('terms') and Input::get('terms') != ''){
      $terms = Input::get('terms');

      $images = App\Image::whereRaw('tags REGEXP ?', array($terms))->get();
      $count = $images->count();
   }

   return [
      "images"=>$images,
      "total_count"=>$count
   ];
});

Route::get('allEvents', function(){
   $events = App\Event::all();

   return $events;
});

Route::get('allTags', function(){
   $tags = App\Tag::orderBy('tag')->get();

   return $tags;
});

function toByteSize($p_sFormatted) {
    $aUnits = array('B'=>0, 'K'=>1, 'M'=>2, 'G'=>3);
    $sUnit = strtoupper(trim(substr($p_sFormatted, -1)));
    if (intval($sUnit) !== 0) {
        $sUnit = 'B';
    }
    if (!in_array($sUnit, array_keys($aUnits))) {
        return false;
    }
    $iUnits = trim(substr($p_sFormatted, 0, strlen($p_sFormatted) - 1));
    if (!intval($iUnits) == $iUnits) {
        return false;
    }
    return $iUnits * pow(1024, $aUnits[$sUnit]);
}
