<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model {
   protected $table = 'images';
   protected $fillable = ['url', 'description', 'tags'];
   protected $guarded = array('id');
}
