
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `batts` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `batts`;
DROP TABLE IF EXISTS `event_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event_types` (
  `id` int(11) NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `color` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '#86C441'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `event_types` WRITE;
/*!40000 ALTER TABLE `event_types` DISABLE KEYS */;
INSERT INTO `event_types` VALUES (0,'club','#87c440'),(1,'event','#E24067'),(2,'party','#2da4c2');
/*!40000 ALTER TABLE `event_types` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `events` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `start` datetime NOT NULL,
  `end` datetime NOT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `event_type_id` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `events` WRITE;
/*!40000 ALTER TABLE `events` DISABLE KEYS */;
INSERT INTO `events` VALUES (1,'Club meeting 1','A test club meeting','2015-10-23 06:56:32','2015-10-23 08:56:32','club',0,'2015-10-23 13:56:32','2015-10-23 13:56:32');
/*!40000 ALTER TABLE `events` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `images` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tags` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `images` WRITE;
/*!40000 ALTER TABLE `images` DISABLE KEYS */;
INSERT INTO `images` VALUES (1,'./images/gallery/what+do+eastern+box+turtleseat.jpg','Lorem ipsum dolor sit amet, ac ullamcorper et aliquam, faucibus elit cras massa volutpat, etiam ultricies ut aenean aliquam aptent cursus, eros pharetra tempus aliquam quis sollicitudin.','turtle','2015-10-23 13:56:30','2015-10-23 13:56:30'),(2,'./images/gallery/EasternBoxTurtleMale.jpg','Lorem ipsum dolor sit amet, ac ullamcorper et aliquam, faucibus elit cras massa volutpat, etiam ultricies ut aenean aliquam aptent cursus, eros pharetra tempus aliquam quis sollicitudin.','turtle','2015-10-23 13:56:30','2015-10-23 13:56:30'),(3,'./images/gallery/Terrapene_ornata_male.jpg','Lorem ipsum dolor sit amet, ac ullamcorper et aliquam, faucibus elit cras massa volutpat, etiam ultricies ut aenean aliquam aptent cursus, eros pharetra tempus aliquam quis sollicitudin.','turtle','2015-10-23 13:56:30','2015-10-23 13:56:30'),(4,'./images/gallery/14526715971_0b853310d6_z.jpg','Lorem ipsum dolor sit amet, ac ullamcorper et aliquam, faucibus elit cras massa volutpat, etiam ultricies ut aenean aliquam aptent cursus, eros pharetra tempus aliquam quis sollicitudin.','turtle','2015-10-23 13:56:31','2015-10-23 13:56:31'),(5,'./images/gallery/_64140145_01295586.jpg','Lorem ipsum dolor sit amet, ac ullamcorper et aliquam, faucibus elit cras massa volutpat, etiam ultricies ut aenean aliquam aptent cursus, eros pharetra tempus aliquam quis sollicitudin.','turtle','2015-10-23 13:56:31','2015-10-23 13:56:31'),(6,'./images/gallery/Aldabra_Giant_Tortoise_Geochelone_gigantea_edit1.jpg','Lorem ipsum dolor sit amet, ac ullamcorper et aliquam, faucibus elit cras massa volutpat, etiam ultricies ut aenean aliquam aptent cursus, eros pharetra tempus aliquam quis sollicitudin.','turtle','2015-10-23 13:56:31','2015-10-23 13:56:31'),(7,'./images/gallery/0308-Tortoises-W(1).jpg','Lorem ipsum dolor sit amet, ac ullamcorper et aliquam, faucibus elit cras massa volutpat, etiam ultricies ut aenean aliquam aptent cursus, eros pharetra tempus aliquam quis sollicitudin.','tortoise|sulcata','2015-10-23 13:56:31','2015-10-23 13:56:31'),(8,'./images/gallery/african-spurred-tortoise1.jpg','Lorem ipsum dolor sit amet, ac ullamcorper et aliquam, faucibus elit cras massa volutpat, etiam ultricies ut aenean aliquam aptent cursus, eros pharetra tempus aliquam quis sollicitudin.','tortoise|sulcata','2015-10-23 13:56:31','2015-10-23 13:56:31'),(9,'./images/gallery/African-Sulcata-Tortoise.jpg','Lorem ipsum dolor sit amet, ac ullamcorper et aliquam, faucibus elit cras massa volutpat, etiam ultricies ut aenean aliquam aptent cursus, eros pharetra tempus aliquam quis sollicitudin.','tortoise|sulcata','2015-10-23 13:56:31','2015-10-23 13:56:31'),(10,'./images/gallery/ceec48a5355c682c324450b66b033515.jpg','Lorem ipsum dolor sit amet, ac ullamcorper et aliquam, faucibus elit cras massa volutpat, etiam ultricies ut aenean aliquam aptent cursus, eros pharetra tempus aliquam quis sollicitudin.','tortoise|sulcata','2015-10-23 13:56:31','2015-10-23 13:56:31'),(11,'./images/gallery/DSCN2975.jpg','Lorem ipsum dolor sit amet, ac ullamcorper et aliquam, faucibus elit cras massa volutpat, etiam ultricies ut aenean aliquam aptent cursus, eros pharetra tempus aliquam quis sollicitudin.','tortoise|sulcata','2015-10-23 13:56:31','2015-10-23 13:56:31'),(12,'./images/gallery/Geochelone_sulcata_-Oakland_Zoo_-feeding-8a.jpg','Lorem ipsum dolor sit amet, ac ullamcorper et aliquam, faucibus elit cras massa volutpat, etiam ultricies ut aenean aliquam aptent cursus, eros pharetra tempus aliquam quis sollicitudin.','tortoise|sulcata','2015-10-23 13:56:31','2015-10-23 13:56:31'),(13,'./images/gallery/p00yfk3x.jpg','Lorem ipsum dolor sit amet, ac ullamcorper et aliquam, faucibus elit cras massa volutpat, etiam ultricies ut aenean aliquam aptent cursus, eros pharetra tempus aliquam quis sollicitudin.','tortoise|sulcata','2015-10-23 13:56:31','2015-10-23 13:56:31'),(14,'./images/gallery/sulcata-tortoise-tank.jpg','Lorem ipsum dolor sit amet, ac ullamcorper et aliquam, faucibus elit cras massa volutpat, etiam ultricies ut aenean aliquam aptent cursus, eros pharetra tempus aliquam quis sollicitudin.','tortoise|sulcata','2015-10-23 13:56:31','2015-10-23 13:56:31'),(15,'./images/gallery/sulcata-tortoise.jpg','Lorem ipsum dolor sit amet, ac ullamcorper et aliquam, faucibus elit cras massa volutpat, etiam ultricies ut aenean aliquam aptent cursus, eros pharetra tempus aliquam quis sollicitudin.','tortoise|sulcata','2015-10-23 13:56:31','2015-10-23 13:56:31');
/*!40000 ALTER TABLE `images` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES ('2014_10_12_000000_create_users_table',1),('2014_10_12_100000_create_password_resets_table',1),('2015_10_20_032955_create_images_table',1),('2015_10_23_005752_create_events_table',1),('2015_10_23_063006_create_tags_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tag` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `tags` WRITE;
/*!40000 ALTER TABLE `tags` DISABLE KEYS */;
INSERT INTO `tags` VALUES (1,'club'),(2,'event'),(3,'party'),(4,'turtle'),(5,'tortoise'),(6,'habitat');
/*!40000 ALTER TABLE `tags` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `firstname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

