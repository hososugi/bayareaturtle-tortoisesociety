@extends('layout')

@section('title','Calendar')

@section('styles')
   <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.4.0/fullcalendar.min.css" />
@stop

@section('scripts')
   <script src="http://momentjs.com/downloads/moment.min.js"></script>
   <script src="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.4.0/fullcalendar.min.js"></script>

   <script src="./scripts/calendar.js"></script>
   <script>
      var events = {!!$events!!};
   </script>
@stop

@section('content')
   <div class="col-xs-12 panel">
      <div id='calendar'></div>
   </div>

   <div id="event-viewer" class="modal fade viewer-modal">
      <div class="modal-dialog">
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body text-left clearfix">
               <h3 id="event-viewer-title" class="col-xs-12 no-margin-top no-padding" class="viewer-title">Some Title</h3>
               <div class="col-xs-12 no-padding">
                  <div class="pull-left" style="margin-right: 5px;">Date: </div>
                  <div id="event-viewer-date" class="viewer-title pull-left"></div>
               </div>
               <div id="event-viewer-description" class="col-xs-12 no-padding" class="viewer-description">Some Description</div>
               <div id="event-viewer-map" class="map-wrapper"></div>
               <a id="event-viewer-location" class="col-xs-12 no-padding" href="#" target="_blank">
                  <img id="event-viewer-icon" src="./images/google_maps_icon_32x32.png" alt="Google Maps Link" />
                  <span id="event-viewer-location-text"></span>
               </a>
            </div>
            <div class="modal-footer hidden">
               <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
         </div>
      </div>
   </div>
@stop
