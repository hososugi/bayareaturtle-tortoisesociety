@extends('layout')

@section('title','Resources')

@section('scripts')
   <script src="./scripts/resources.js"></script>
@stop

@section('content')
   <div class="col-md-12 panel clearfix">
      <div class="col-xs-12 col-md-6 no-padding-left">
         <input id="gallery-search-input" type="text" class="form-control inline-icon" placeholder="Type something like: tortoise, care, or habitat">
         <span id="gallery-search-icon" class="glyphicon glyphicon-search floating-search-icon" aria-hidden="true"></span>
      </div>
      <div id="gallery-search-count" class="col-xs-12 col-md-6 no-padding-left search-count">
         {{$loadedCount}} of {{$totalCount}} results
      </div>
   </div>

   <div class="col-xs-12 panel">
      <div id="gallery-images-container" class="col-xs-12 no-padding moments-container">
         @foreach($resources as $resource)
            <div class="event-wrapper col-xs-12 no-padding-horizontal" data-id={{$resource->id}} title="{{$resource->description}}">
               <a href="{{$resource->url}}" target="_blank" class="col-xs-12 no-padding">
                  <div class="resource-icon pull-left">
                     <img src="./images/icon_resource_download.png" alt="{{$resource->title}}" class="icon" />
                  </div>
                  <div class="event-title-wrapper pull-left">{{$resource->title}}</div>
               </a>
            </div>
         @endforeach
      </div>
      <div class="col-xs-12 text-center">
         <button id="gallery-load-more" type="button" class="btn btn-default {{($loadedCount >= $totalCount)? 'disabled' : ''}}">Load More</button>
      </div>
   </div>

   <div id="image-viewer" class="modal fade viewer-modal">
      <div class="modal-dialog">
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body text-center clearfix">
               <img id="image-viewer-img" src="./images/icon_no_spacing_122x122.png" alt="Bay Area Turtle & Tortoise Society logo" class="image-viewer-image" />
               <div class="col-xs-12 text-left image-viewer-tags no-padding-left">Tags: <span id="image-viewer-tags" class="no-padding-right"></span></div>
               <div id="image-viewer-description" class="col-xs-12 text-left image-viewer-description no-padding-horizontal">Some Description</div>
            </div>
            <div class="modal-footer hidden">
               <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
         </div>
      </div>
   </div>
@stop
