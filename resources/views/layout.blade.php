<!DOCTYPE html>
<html lang="en">
<head>
   <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
   <meta charset="utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">

   @if($header_tab != "home")
   <meta name="robots" content="noindex">
   <meta name="googlebot" content="noindex">
   @endif

   <title>Bay Area Tortuga Society - @yield('title')</title>
   <link rel="shortcut icon" type="image/png" href="./images/icon_4.png" />
   <link rel="stylesheet" type="text/css" hreflang="es" href='./css/font-Open-Sans-Condensed.css?family=Open+Sans+Condensed:300' rel='stylesheet' type='text/css'>
   <link rel="stylesheet" type="text/css" hreflang="es" href="./css/font-Lato.css?family=Lato:100">
   <link rel="stylesheet" type="text/css" hreflang="es" href="./libs/jQuery/themes/smoothness/jquery-ui.css">
   <link rel="stylesheet" type="text/css" hreflang="es" href="./libs/bootstrap/css/bootstrap.min.css">
   <link rel="stylesheet" type="text/css" hreflang="es" href="./libs/bootstrap/css/bootstrap-theme.min.css">

   <link href="{!! Less::url('app') !!}" rel="stylesheet" />

   @yield('styles')

   <script src="./libs/jQuery/jquery-2.1.4.min.js"></script>
   <script src="./libs/jQuery/jquery-ui.min.js"></script>
   <script src="./libs/bootstrap/js/bootstrap.min.js"></script>
   <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>

   <script src="./scripts/app.js"></script>
   @yield('scripts')
</head>

<body>
   <header>
      <div class="container">
         <div class="col-xs-10 col-sm-6 no-padding-left">
            <a href="{{url('/')}}">
               <div class="hidden-xs">
                  <div id="logo-text">
                     <img class="logo text-sm-center text-xs-center" src="./images/logo2.png" alt="Bay Area Tortuga Society Logo" />
                     <span id="header-bayArea" class="col-xs-12 no-padding-left">Bay Area</span>
                     <span id="header-tortuga" class="col-xs-12 no-padding-left">Tortuga</span>
                     <span id="header-society" class="col-xs-12 no-padding-left">Society</span>
                  </div>
               </div>
               <div class="visible-xs">
                  <div id="logo-text">
                     <img class="logo text-sm-center text-xs-center" src="./images/logo2.png" alt="Bay Area Tortuga Society Logo" />
                     <span id="header-bayArea" class="col-xs-12 no-padding-left">Bay Area</span>
                     <span id="header-tortuga">Tortuga</span>
                     <span id="header-society">Society</span>
                  </div>
               </div>
            </a>
         </div>
         <div class="col-xs-2 visible-xs no-padding-left clearfix" role="navigation">
            <div class="navbar-header navbar-hamburger" data-toggle="collapse" data-target="#main-navbar">
               <button type="button" class="navbar-toggle collapsed no-margin-horizontal" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
               </button>
            </div>
         </div>
         <div class="col-xs-12 col-sm-6 no-padding-left clearfix" role="navigation">
            <div id="main-navbar" class="collapse navbar-collapse navbar-right">
               <ul class="nav navbar-header">
                  <li role="presentation" class={{ $header_tab == "home"? "active" : ""}}><a href="{{url('/')}}">Home</a></li>
                  <li role="presentation" class={{ ($header_tab == "gallery"? "active" : "")}}><a href="{{url('/gallery')}}">Gallery</a></li>
                  <li role="presentation" class={{ ($header_tab == "calendar"? "active" : "")}}><a href="{{url('/calendar')}}">Calendar</a></li>
                  <li role="presentation" class={{ ($header_tab == "resources"? "active" : "")}}><a href="{{url('/resources')}}">Resources</a></li>
                  <li role="presentation" class={{ ($header_tab == "about"? "active" : "")}}><a href="{{url('/about')}}">About</a></li>
                  {{--
                  <li class="divider-vertical"></li>
                  <li role="presentation" class=""><a href="#">Log in</a></li>
                  --}}
               </ul>
            </div>
         </div>
      </div>
   </header>

   <div class="container">
      @yield('content')
   </div>

   @include('footer')
</body>
</html>
