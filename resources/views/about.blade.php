@extends('layout')

@section('title','Resources')

@section('scripts')
   <script src="./scripts/about.js"></script>
@stop

@section('content')


<div class="col-xs-12 panel">
   <div class="col-xs-12">
      <div class="col-xs-12">
         <h3 class="no-margin-top">About Us</h3>
         <p>Bay Area Turtle & Tortoise Society is dedicated to turtle & tortoise education, facilitating the care, rescue and adoption of turtles and tortoises throughout the bay area of all nonnative and native species.</p>
         <p>Our club offers monthly meetings that typically include an educational program and a chance to socialize and share information among turtle & tortoise keepers from the bay area. Our club also will sponsor field trips for 2016, and participate in community outreach activities. All club members receive the club newsletter every other month.</p>
      </div>

      <div class="col-xs-12">
         <h3 class="no-margin-top">Officers</h3>

         <div class="col-xs-12 col-md-6">
            <div class="col-xs-3">President</div>
            <div class="col-xs-9">Johnny Rodriguez</div>
         </div>

         <div class="col-xs-12 col-md-6">
            <div class="col-xs-3">Vice President</div>
            <div class="col-xs-9">Steven Sifuentes</div>
         </div>

         <div class="col-xs-12 col-md-6">
            <div class="col-xs-3">Treasurer</div>
            <div class="col-xs-9">-</div>
         </div>

         <div class="col-xs-12 col-md-6">
            <div class="col-xs-3">Secretary</div>
            <div class="col-xs-9">-</div>
         </div>
      </div>

      <div class="col-xs-12">
         <h3 class="no-margin-top">Club meeting location</h3>
         <div id="event-viewer-map" class="map-wrapper"></div>
         <a id="event-viewer-location" class="col-xs-12 no-padding" href="https://www.google.com/maps/search/San%20Jose%20Masonic%20Center%202500%20Masonic%20Dr,%20San%20Jose,%20CA%2095125" target="_blank">
            <img id="event-viewer-icon" src="./images/google_maps_icon_32x32.png" alt="Google Maps Link" />
            <span id="event-viewer-location-text">San Jose Masonic Center 2500 Masonic Dr, San Jose, CA 95125</span>
         </a>
      </div>
</div>
</div>
@stop
