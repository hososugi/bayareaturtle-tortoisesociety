<footer>
   <div class="container text-center">
      Copyright &copy; {{date("Y")}} Johnny Rodriguez.
      <a href="mailto:BayAreaTortugaSociety@gmail.com">
         <img class="icon" src="{{url('/')}}/images/email_circle_grey_128.png" alt="email icon" />
      </a>
      <a href="https://www.facebook.com/groups/1706171792938074/">
         <img class="icon" src="{{url('/')}}/images/facebook_circle_grey_128.png" alt="email icon" />
      </a>
   </div>
</footer>
