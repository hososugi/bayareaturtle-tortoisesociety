@extends('admin/admin_login_layout')

@section('title','Admin Dashboard')

@section('scripts')
@stop

@section('content')
   <h3 class="no-margin-top">Login</h3>
   <form method="POST" action="{{url('/')}}/auth/login">
      {!! csrf_field() !!}

      <div class="col-xs-12 col-sm-6">
         <div class="row row-margin">
            <div class="col-xs-12 col-sm-2">
               Email
            </div>
            <div class="col-xs-12 col-sm-10">
               <input id="login-username" type="email" name="email" value="{{ old('email') }}" class="form-control">
            </div>
         </div>

         <div class="row row-margin">
            <div class="col-xs-12 col-sm-2">
               Password
            </div>
            <div class="col-xs-12 col-sm-10">
               <input id="login-password" type="password" name="password" class="form-control">
            </div>
         </div>

         <div class="row">
            <div class="col-xs-12 col-sm-5 col-sm-offset-2">
               <input type="checkbox" name="remember"> Remember Me
            </div>
            <div class="col-xs-12 col-sm-5 text-right">
               <button type="submit" class="btn btn-default">Login</button>
            </div>
         </div>
      </div>
   </form>
@stop
