@extends('admin/admin_login_layout')

@section('title','Admin Dashboard')

@section('scripts')
@stop

@section('content')
   <h3 class="no-margin-top">Login</h3>
   <form method="POST" action="/auth/login">
      {!! csrf_field() !!}

      <div class="row">
         <div class="col-xs-12 col-sm-5">
            Email
         </div>
         <div class="col-xs-12 col-sm-7">
            <input type="email" name="email" value="{{ old('email') }}">
         </div>
      </div>

      <div class="row">
         Password
         <input type="password" name="password" id="password">
      </div>

      <div class="row">
         <input type="checkbox" name="remember"> Remember Me
      </div>

      <div class="row">
         <button type="submit">Login</button>
      </div>
   </form>
@stop
