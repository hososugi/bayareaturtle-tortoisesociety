@extends('admin/admin_layout')

@section('title','Admin Dashboard')

@section('styles')
   <link rel="stylesheet" href="{{url('/')}}/libs/DataTables-1.10.9/css/dataTables.bootstrap.min.css">
@stop

@section('scripts')
   <script src="{{url('/')}}/libs/bootstrap-filestyle/bootstrap-filestyle.min.js"></script>
   <script src="{{url('/')}}/libs/DataTables-1.10.9/js/jquery.dataTables.min.js"></script>
   <script src="{{url('/')}}/scripts/admin/images.js"></script>
@stop

@section('content')
   <div class="col-xs-12 col-sm-6 no-padding-left">
      <h3 class="no-margin-top">
         Images
         <span id="admin-images-count">({{$loadedCount}} of {{$totalCount}} images)</span>
      </h3>
   </div>

   <div id="admin-images-upload-form" class="col-xs-12 col-sm-6 no-padding-right">
      {!! Form::open(array('id'=>'image-upload-form', 'route'=>'admin.imageUpload','method'=>'POST', 'files'=>true)) !!}
         <div class="control-group">
            <div class="controls">
               <input type="hidden" name="MAX_FILE_SIZE" value="{{$max_file_size}}" />
               {!! Form::file('image', ['id'=>'image-upload-input', 'class'=>'filestyle pull-right', 'data-input'=>"false", 'data-buttonText'=>"&nbsp;Add image", 'data-icon'=>'glyphicon glyphicon-picture']) !!}
            </div>
         </div>
         {!! Form::submit('Upload', array('class'=>'send-btn hidden')) !!}
      {!! Form::close() !!}
   </div>

   <div class="col-xs-12 no-padding">
      <table id="admin-images-table" class="table">
         <thead>
            <tr>
               <th></th>
               <th>Description</th>
               <th>Tags</th>
               <th></th>
            </tr>
         </thead>
         <tbody>
            @foreach($images as $image)
               <tr class="moments-container">
                  <td>
                     <div class="moment-wrapper">
                        <img class="moment-image" src="{{url('/')}}{{substr($image->url,1)}}" />
                     </div>
                  </td>
                  <td>
                     <div class="moment-description truncate">
                        {{$image->description}}
                     </div>
                  </td>
                  <td>
                     <?php
                        $tabsArray = explode('|', $image->tags);
                        echo implode(', ', $tabsArray);
                     ?>
                  </td>
                  <td>
                     <span class="glyphicon glyphicon-pencil" aria-hidden="true" data-id="{{$image->id}}" data-url="{{url('/')}}{{substr($image->url,1)}}" data-tags="{{$image->tags}}" data-description="{{$image->description}}"></span>
                     <span class="glyphicon glyphicon-trash" aria-hidden="true" data-id={{$image->id}}></span>
                  </td>
               </tr>
            @endforeach
         </tbody>
      </table>
   </div>

<div id="modal-remove-image" class="modal fade">
   <div class="modal-dialog modal-sm">
      <div class="modal-content">
      <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
         <h4 class="modal-title">Remove Image</h4>
      </div>
      <div class="modal-body">
         <p>Are you sure you want to remve the image? It will be permanent.</p>
      </div>
      <div class="modal-footer">
         <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
         <button id="modal-remove-image-button" type="button" class="btn btn-danger">Remove</button>
      </div>
      </div>
   </div>
</div>

<div id="modal-edit-image" class="modal fade image-viewer-modal">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
         </div>
         <div class="modal-body text-center clearfix">
            <img id="modal-edit-image-img" src="{{url('/')}}/images/icon_no_spacing_122x122.png" alt="Bay Area Turtle & Tortoise Society logo" class="image-viewer-image" />

            <div class="col-xs-12 no-padding text-left">
               <label for="model-edit-image-tags">Tags:</label>
               <input id="model-edit-image-tags" type="text" class="form-control" placeholder="A comma-separated list">
            </div>

            <div class="col-xs-12 no-padding text-left">
               <label for="model-edit-image-description">Description:</label>
               <textarea id="model-edit-image-description" type="text" class="form-control" aria-describedby="basic-addon3"></textarea>
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            <button id="modal-update-image-button" type="button" class="btn btn-primary" disabled>Update</button>
         </div>
      </div>
   </div>
</div>
@stop
