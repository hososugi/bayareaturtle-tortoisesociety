@extends('admin/admin_layout')

@section('title','Admin Dashboard')

@section('styles')
@stop

@section('scripts')
   <script src="{{url('/')}}/scripts/admin/dashboard.js"></script>
@stop

@section('content')
   <h3 class="no-margin-top">Dashboard</h3>
@stop
