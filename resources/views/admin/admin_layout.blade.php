<?php
   $encrypter = app('Illuminate\Encryption\Encrypter');
   if(!isset($header_tab)){
      $header_tab = 'admin';
   }
   if(!isset($header_admin_tab)){
      $header_admin_tab = '';
   }
?>
<!DOCTYPE html>
<html lang="en">
<head>
   <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
   <meta charset="utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">

   <meta name="robots" content="noindex">
   <meta name="googlebot" content="noindex">

   <title>Bay Area Turtle & Tortoise Society - @yield('title')</title>

   <link rel="shortcut icon" type="image/png" href="./images/icon_4.png" />
   <link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300' rel='stylesheet' type='text/css'>
   <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
   <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">

   <link href="{!! Less::url('app') !!}" rel="stylesheet" />

   @yield('styles')

   <script src="//code.jquery.com/jquery-2.1.4.min.js"></script>
   <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
   <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

   <script src="{{url('/')}}/scripts/app.js"></script>
   <script>
      var csrf = "{{$encrypter->encrypt(csrf_token())}}";
   </script>
   @yield('scripts')
</head>

<body>
   <header>
      <div class="container">
         <div class="col-xs-10 col-sm-5 col-md-6 no-padding">
            <a href="{{url('/')}}">
               <div class="hidden-xs">
                  <span id="header-subtext">Bay Area</span>
                  <span id="header-maintext">Turtle & Tortoise Society</span>
               </div>
               <div class="visible-xs">
                  <span id="header-subtext" class="col-xs-12 no-padding-left">Bay Area</span>
                  <span id="header-maintext">Turtle & Tortoise Society</span>
               </div>
            </a>
         </div>
         <div class="col-xs-2 visible-xs no-padding-left clearfix" role="navigation">
            <div class="navbar-header navbar-hamburger" data-toggle="collapse" data-target="#main-navbar">
               <button type="button" class="navbar-toggle collapsed no-margin-horizontal" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
               </button>
            </div>
         </div>
         <div class="col-xs-12 col-sm-7 col-md-6 no-padding-left clearfix" role="navigation">
            <div id="main-navbar" class="collapse navbar-collapse">
               <ul class="nav navbar-header">
                  <li role="presentation" class={{ $header_tab == "home"? "active" : ""}}><a href="{{url('/')}}">Home</a></li>
                  <li role="presentation" class={{ ($header_tab == "gallery"? "active" : "")}}><a href="{{url('/gallery')}}">Gallery</a></li>
                  <li role="presentation" class={{ ($header_tab == "calendar"? "active" : "")}}><a href="{{url('/calendar')}}">Calendar</a></li>
                  <li role="presentation" class={{ "disabled ".($header_tab == "resources"? "active" : "")}}><a href="#">Resources</a></li>
                  <li role="presentation" class={{ "disabled ".($header_tab == "about"? "active" : "")}}><a href="#">About</a></li>
                  <li role="presentation" class={{ ($header_tab == "admin"? "active" : "")}}><a href="{{url('/admin')}}">Admin</a></li>
               </ul>
            </div>
         </div>
      </div>
   </header>

   <div class="container">
      <div id="admin-user-wrapper" class="col-xs-12 no-padding text-right">
         @if(Auth::user() !== null)
         <div class="dropdown">
            <div id="dropdown-admin-logout" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
               {{Auth::user()->firstname}}
               <span class="caret"></span>
            </div>
            <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdown-admin-logout">
               <li><a href="{{url('/').'/auth/logout'}}">Logout</a></li>
            </ul>
         </div>
         @endif
      </div>

      <div class="col-xs-12 col-sm-2 no-padding-left">
         <div class="col-xs-12 no-padding panel">
            <ul class="nav nav-pills nav-stacked navbar-side">
               <!--<li role="presentation" class="text-right">
                  <a href="#"><span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span></a>
               </li>-->
               <li role="presentation" class={{ $header_admin_tab == "dashboard"? "active" : ""}}>
                  <a href="{{url('admin')}}"><span class="glyphicon glyphicon-stats" aria-hidden="true"></span> Dashboard</a>
               </li>
               <li role="presentation" class={{ ($header_admin_tab == "images"? "active" : "")}}>
                  <a href="{{url('admin/images')}}"><span class="glyphicon glyphicon-picture" aria-hidden="true"></span> Images</a>
               </li>
               <li role="presentation" class={{ ($header_admin_tab == "events"? "active" : "")}}>
                  <a href="{{url('admin/events')}}"><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span> Events</a>
               </li>
               <li role="presentation" class={{ ($header_admin_tab == "resources"? "active" : "")}}>
                  <a href="{{url('admin/resources')}}"><span class="glyphicon glyphicon-book" aria-hidden="true"></span> Resources</a>
               </li>
            </ul>
         </div>
      </div>

      <div class="col-xs-12 col-sm-10 no-padding-right">
         <div class="col-xs-12 panel">
            @yield('content')
         </div>
      </div>
   </div>

   @include('footer')
</body>
</html>
