@extends('layout')

@section('title','Home')

@section('scripts')
   <script src="./scripts/home.js"></script>
   <script>
      var events = {!!$events!!};
      var resources = {!!$resources!!};
   </script>
@stop

@section('content')
   <div class="col-xs-12 panel text-center hidden">
      <img class="icon" src="./images/construction_128x128.png" alt="construction icon"></span>
      This website is under construction and is undergoing a name change. The site will remain here until the new site is ready.
   </div>

   <div id="panel-welcome" class="row">
      <div class="col-xs-12 col-md-12">
         <h3 class="no-margin-top">Welcome</h3>
         <p>Bay Area Tortuga Society is dedicated to turtle & tortoise education, facilitating the care, rescue and adoption of turtles and tortoises throughout the bay area of all nonnative and native species.</p>
         <p>Our club offers monthly meetings that typically include an educational program and a chance to socialize and share information among turtle & tortoise keepers from the bay area. Our club also will sponsor field trips for 2016, and participate in community outreach activities. All club members receive the club newsletter every other month.</p>
      </div>
   </div>

   <div id="panel-updates" class="col-xs-12 panel">
      <div class="col-xs-12 col-md-12">
         <h3 class="no-margin-top">
            <div class="event-wrapper col-xs-12 no-padding-horizontal" data-id="0" onClick="showEventDetails(0);">
               <div class="event-date-wrapper">
                  {{date('M d', strtotime("2016-02-19 19:00:00"))}}
               </div>
               <div class="event-title-wrapper" title="Speaker: Eliza Trickett">
                  Speaker: Eliza Trickett
               </div>
            </div>
            <div style="clear:both;"></div>
         </h3>

         <div class="col-xs-12 col-md-8 no-padding-horizontal">
            <p>
               Regional Sale Manager, Mazuri&reg; Exotic
            </p>
            <p>
               Eliza provides technical product and sales support to zoos, aquariums, veterinarians, private exotic animal owners, the Purina retail sales team, and Purina feed dealers. her territory covers western United States, Canada, and Brazil!
            </p>
            <p>
               Eliza is one of the 4 Mazuri dedicated professionals in the field. She obtained her bachelor’s degree from University of Sao Paulo in animal science, including one year studying ruminant nutrition at Texas A&M University, College Station. Since graduation her focus has always been on nutrition. She participated on the Exotic Animal Nutrition Internship at San Diego Zoo, managed diets for Altamira Vivencias equestrian school in Brazil and worked for the Brazilian Bonsmara Breeders Association. Her diversity and multi-cultural experience within the industry will complement the existing excellence already established by the Mazuri team.
            </p>
         </div>
         <div class="col-xs-12 col-md-4 text-center">
            <img src="./images/eliza_trickett.png" alt="Speaker Eliza Trickett" />
         </div>

      </div>
   </div>

   <div class="row">
      <div class="col-xs-12 col-md-4">
         <div class="col-xs-12 panel home-small-panel no-padding">
            <div class="col-xs-12 panel-heading">
               Moments
            </div>
            <div class="col-xs-12 panel-content">
               <div id="home-moments-wrapper" class="col-xs-12 no-padding">
                  <div id="home-moments" class="moments-container">
                     @foreach($images as $image)
                        <div class="moment-wrapper" data-id={{$image->id}} data-url={{$image->url}} data-tags="{{$image->tags}}" data-description="{{$image->description}}" onClick="showImageDetails(this);">
                           <img class="moment-image" src={{$image->url}} alt={{$image->url}} />
                        </div>
                     @endforeach
                  </div>
               </div>
               <div id="home-moments-buttons" class="col-xs-12 no-padding">
                  <div id="home-moment-bubble-buttons" class="col-xs-6 no-padding-left moment-bubble-buttons">
                     @for($i = 0; $i < count($images); $i++)
                        @if($i == 0)
                           <div class="moment-bubble-button pull-left active" data-index={{$i}}></div>
                        @else
                           <div class="moment-bubble-button pull-left" data-index={{$i}}></div>
                        @endif
                     @endfor
                  </div>
                  <div class="col-xs-6 no-padding-right text-right">
                     <a href="./gallery">More</a>
                  </div>
               </div>
            </div>
         </div>
      </div>

      <div class="col-xs-12 col-md-4">
         <div class="col-xs-12 panel home-small-panel no-padding">
            <div class="col-xs-12 panel-heading">
               Upcoming Events & Meetings
            </div>
            <div class="col-xs-12 panel-content">
               @foreach($events as $key=>$event)
                  <div class="event-wrapper col-xs-12 no-padding-horizontal" data-id={{$event->id}} onClick="showEventDetails({{$key}});">
                     <div class="event-date-wrapper">
                        {{date('M d', strtotime($event->start))}}
                     </div>
                     <div class="event-title-wrapper" title="{{$event->title}}">
                        {{$event->title}}
                     </div>
                  </div>
               @endforeach
            </div>
            <div class="col-xs-12 text-right">
               <a href="./calendar">More</a>
            </div>
         </div>
      </div>

      <div class="col-xs-12 col-md-4">
         <div class="col-xs-12 panel home-small-panel no-padding">
            <div class="col-xs-12 panel-heading">
               Popular Resources
            </div>
            <div class="col-xs-12 panel-content">
               @foreach($resources as $key=>$resource)
                  <div class="event-wrapper col-xs-12 no-padding-horizontal" data-id={{$resource->id}} title="{{$resource->description}}" >
                     <a href="{{$resource->url}}" target="_blank" class="col-xs-12 no-padding">
                        <div class="resource-icon pull-left">
                           <img src="./images/icon_resource_download.png" alt="{{$resource->title}}" class="icon" />
                        </div>
                        <div class="event-title-wrapper pull-left">{{$resource->title}}</div>
                     </a>
                  </div>
               @endforeach
            </div>
            <div class="col-xs-12 text-right">
               <a href="./resources">More</a>
            </div>
         </div>
      </div>
   </div>

   <div id="image-viewer" class="modal fade viewer-modal">
      <div class="modal-dialog">
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body text-center clearfix">
               <img id="image-viewer-img" src="./images/icon_no_spacing_122x122.png" alt="Bay Area Turtle & Tortoise Society logo" class="image-viewer-image" />
               <div class="col-xs-12 text-left image-viewer-tags no-padding-left">Tags: <span id="image-viewer-tags" class="no-padding-right"></span></div>
               <div id="image-viewer-description" class="col-xs-12 text-left image-viewer-description no-padding-horizontal">Some Description</div>
            </div>
            <div class="modal-footer hidden">
               <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
         </div>
      </div>
   </div>

   <div id="event-viewer" class="modal fade viewer-modal">
      <div class="modal-dialog">
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body text-left clearfix">
               <h3 id="event-viewer-title" class="col-xs-12 no-margin-top no-padding" class="viewer-title">Some Title</h3>
               <div class="col-xs-12 no-padding">
                  <div class="pull-left" style="margin-right: 5px;">Date: </div>
                  <div id="event-viewer-date" class="viewer-title pull-left"></div>
               </div>
               <div id="event-viewer-description" class="col-xs-12 no-padding" class="viewer-description">Some Description</div>
               <div id="event-viewer-map" class="map-wrapper"></div>
               <a id="event-viewer-location" class="col-xs-12 no-padding" href="#" target="_blank">
                  <img id="event-viewer-icon" src="./images/google_maps_icon_32x32.png" alt="Google Maps Link" />
                  <span id="event-viewer-location-text"></span>
               </a>
            </div>
            <div class="modal-footer hidden">
               <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
         </div>
      </div>
   </div>
@stop
